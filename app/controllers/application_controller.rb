class ApplicationController < ActionController::API
    include WardenHelper

    rescue_from StandardError, with: :render_unknown_error
    rescue_from ActiveRecord::RecordNotFound,       with: :not_found
    rescue_from ActionController::ParameterMissing, with: :missing_param_error

    def render_unknown_error(error)
        render(json: { error: error.message }, status: 500)
    end

    def not_found
        render status: :not_found, json: ''
    end

    def missing_param_error(exception)
        render status: :unprocessable_entity, json: { error: exception.message }
    end
end
