class ErrorsController < ApplicationController
  skip_before_action :authenticate!

  def unknown(error)
    render(json: { error: error.message }, status: 500)
  end

  def not_found
      render status: :not_found, json: ''
  end

  def missing_param(exception)
      render status: :unprocessable_entity, json: { error: exception.message }
  end

end
