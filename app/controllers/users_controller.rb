class UsersController < ApplicationController
    before_action :set_user, only: [:show, :update, :destroy]

    # Listado de usuarios registrados con paginación
    def index
      params[:page].nil? || params[:page].to_i  < 1 ? page = 1 : page = params[:page].to_i
      @users = User.paginate(:page => page, :per_page => 5)
  
      # page += 1
      result = Hash.new
      result[:users] = @users
      result[:previous_page] = request.base_url + "/users?page=#{page-1}" unless page == 1
      result[:next_page] = request.base_url + "/users?page=#{page+1}" unless @users.length < 5

      render json: result
    end
  
    # Muestra datos de un usuario específico
    def show
      render json: @user
    end
  
    # Registra nuevos usuarios, 
    # Solo usuarios registrados y con sesión iniciada pueden crear nuevos usuarios
    def create
      @user = User.new(user_params)
  
      if @user.save
        render json: @user, status: :created, location: @user
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  
    # Actualiza datos de usuarios registrados
    def update
      if @user.update(user_params)
        head :no_content
      else
        render json: @user.errors, status: :unprocessable_entity
      end
    end
  
    # Elimina físicamente usuarios registrados
    def destroy
      @user.destroy
  
      head :no_content
    end
  
    private
  
    def set_user
      @user = User.find(params[:id])
    end
  
    def user_params
      params.require(:user).permit(:name, :email, :birthday, :password, :lastname)
    end
end
