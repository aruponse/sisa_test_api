class User < ActiveRecord::Base
    include Rails.application.routes.url_helpers
    has_many :authentication_tokens
    has_secure_password
    validates :password, length: { minimum: 8 }
    validates :birthday, presence: true
    validates :email, uniqueness: true
end
