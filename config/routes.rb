Rails.application.routes.draw do
  get '/500', to: 'errors#unknown'
  get '/404', to: 'errors#not_found'
  get '/422', to: 'errors#missing_param'

  root 'users#index'
  resource  :sessions,  only: [:create, :destroy]
  resources :users, only: [:index, :show, :create, :update]

  cors_head = proc do
    [
      204,
      {
        'Content-Type'                 => 'text/plain',
        'Access-Control-Allow-Origin'  => CORS_ALLOW_ORIGIN,
        'Access-Control-Allow-Methods' => CORS_ALLOW_METHODS,
        'Access-Control-Allow-Headers' => CORS_ALLOW_HEADERS
      },
      []
    ]
  end
  match '/*path', to: cors_head, via: [:options, :head]
  
end
