require 'rails_helper'

RSpec.describe ErrorsController, type: :controller do

  describe "GET #unknown" do
    it "returns http 500" do
      get :unknown
      expect(response).to have_http_status(:internal_server_error)
    end
  end

  describe "GET #not_found" do
    it "returns http not_found" do
      get :not_found
      expect(response).to have_http_status(:not_found)
    end
  end

  describe "GET #missing_param" do
    it "returns http internal_server_error" do
      get :missing_param
      expect(response).to have_http_status(:internal_server_error)
    end
  end

end
