class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :email, index: :email
      t.string :password_digest
      t.date :birthday
      t.string :name
      t.string :lastname

      t.timestamps null: false
    end
  end
end
